import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';
import { Subscription } from 'rxjs';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/service/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  public isLoggedIn = false;
  public userProfile: KeycloakProfile | null = null;
  private subscription: Subscription = new Subscription;
  user = new User() ;

  constructor(private readonly keycloak: KeycloakService, private _userService: UserService) { }

  public async ngOnInit() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();

    if (this.isLoggedIn) {
      this.userProfile = await this.keycloak.loadUserProfile();
      this.getUserById();
    }
  }
  getUserById(): void {
    this.subscription = this._userService.getUserById().subscribe({
      next: (data: any) => {
        // Handle successful response, if needed
        console.log(data);
        this.user.username= data.username;
        this.user.email= data.email;
      },
      error: (error: any) => {
        console.error('Error getUserById:', error);
        console.error(error);
      },    // errorHandler 
    });
  }

  public login() {
    this.keycloak.login();
  }

  public logout() {
    this.keycloak.logout();
  }
}


